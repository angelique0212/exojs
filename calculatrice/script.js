//----CREATION VARIABLES------------
let number1='';
let number2 = '';
let operator='';
let total='';
let reset='C'
//-----------RECUPERATION VALEURS AU CLICK
$(document).ready(function(){
    $('.btn').on('click', function(event){
       // console.log('event', event.target.innerHTML);
       let button = event.target.innerHTML;
           if(button >= '0' && button <=9){
              // console.log('number');
              gestionNumber(button);
           }
           else{
              // console.log('operator');
              gestionOperator(button);
           }
   });
});

//---------------FONCTION AFFICHAGE SAISIE NOMBRE---
function displayButton(button){
   $('#result').text(button);
}

//-----CREATION FONCTIONS
        //--------MANIPULATION TOUCHES NOMBRES
function gestionNumber(number){
    if (number1 === ''){
        number1 = number;
    }
    else{
        number2 = number;
    }
   displayButton(number); 
};
       //----------MANIPULATION TOUCHES OPERATEURS
function gestionOperator(oper){
   if(operator === ''){
       operator = oper;
   }
   else{
       gestionTotal();
       operator = oper;
   }
};

//-----------TOTAUX-------------
function gestionTotal(){
   switch(operator){
       case '+':
           total = +number1 + +number2;
           displayButton(total);
           break;
       case '-':
           total = +number1 - +number2;
           displayButton(total);
           break;
       case '/':
           total = +number1 / +number2;
           displayButton(total);
           break;
       case '*':
           total = +number1 * +number2;
           displayButton(total);
           break;
   }
   updateVariables();
}

function displayButton(button){
   $('#result').text(button);
}

function updateVariables(){
   number1 = total;
   number2 = '';
}

