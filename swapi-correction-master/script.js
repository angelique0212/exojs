var films = [];

$.ajax('https://swapi.dev/api/films/').done(function (data) {

    films = data.results;

    console.log(films);

    for (i = 0; i < films.length; i++) {
        $('#film').append('<option value="' + i + '">' + films[i].title + '</option>');
    }
});

$('#film').change(function () {

    var film = films[$(this).val()];

    $('#infos').html('<h1>' + film.title + '</h1>' +
        '<p>Réalisateur : ' + film.director + '</p>');
    for (i = 0; i < film.characters.length; i++) {
        $('#infos').append('<p data-ajax="' + film.characters[i] + '">...</p>');

    }

    $('[data-ajax]').each(function (index) {
        $.ajax($(this).attr('data-ajax')).done(function (character) {
            $('[data-ajax="' + character.url + '"]').html(character.name);
        });
    });

});
